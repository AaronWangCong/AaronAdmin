import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import path from 'path';
import vueJsx from '@vitejs/plugin-vue-jsx';

import AutoImport from 'unplugin-auto-import/vite';
import Components from 'unplugin-vue-components/vite';
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers';

export default defineConfig({
  plugins: [
    vue(),
    vueJsx(),
    AutoImport({
      resolvers: [ElementPlusResolver()]
    }),
    Components({
      resolvers: [ElementPlusResolver()]
    })
  ],
  resolve: {
    alias: {
      '@': path.resolve(__dirname, './src')
    }
  },
  server: {
    host: '0.0.0.0',
    port: 3000,
    open: true,
    strictPort: false,
    https: false,
    proxy: {
      '/v1': {
        target: 'http://localhost:3200',
        changeOrigin: true,
        rewrite: path => path.replace(/^\/v1/, 'v1')
      },
      '/oa': {
        target: 'http://node.wangcong.wang',
        changeOrigin: true,
        rewrite: path => path.replace(/^\/oa/, 'oa')
      },
      '/uni': {
        target: 'https://uni.wangcong.wang',
        changeOrigin: true,
        rewrite: path => path.replace(/^\/uni/, 'uni')
      }
    }
  },
  // 打包配置
  base: '/', // 开发或生产环境服务的公共基础路径
  build: {
    brotliSize: false,
    // 消除打包大小超过500kb警告
    chunkSizeWarningLimit: 2000,
    outDir: 'dist', // 指定生成静态资源的存放路径
    assetsDir: 'static' // 指定生成静态资源的存放路径（相对于 build.outDir）
  }
});
