<div align="center">
<p align="center">
  <img alt="logo" src="https://wangcong-1257742121.cos.ap-shanghai.myqcloud.com/media%2Favatar.png" width="120" style="margin-bottom: 10px;">
</p>
<h3 align="center">最新、稳定、简洁后台管理系统</h3>
  <img src="https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square" alt="PRs Welcome">
  <img src="https://img.shields.io/node/v/@tarojs/cli.svg?style=flat-square">
  <img src="https://img.shields.io/npm/v/npm/next-6">
  <img src="https://img.shields.io/npm/l/@tarojs/taro.svg?style=flat-square">
  <img src="https://img.shields.io/badge/version-v1.0.0-yellow">
</div>

#### 项目简介
一个基于 Vue3、typeScript、Vite等 后台管理系统。

**开发文档：**  [http://wcadmin.wangcong.wang](http://wcadmin.wangcong.wang)

**体验地址：**  [http://wcadmin.wangcong.wang](http://wcadmin.wangcong.wang)

**账号密码：** `admin / 123456`
