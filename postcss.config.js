module.exports = {
  plugins: {
    "postcss-import": {},
    "postcss-url": {},
    "postcss-preset-env": {
      features: {
        customProperties: false,
      },
    },
    "postcss-nested": {},
  },
};
