import { constantRouterMap } from '@/router/router';
import Layout from '@/layout/Index.vue';
import ParentView from '@/components/ParentView/index.vue';
import { getToken, setToken, removeToken } from '@/utils/auth';
import Api from '@/lib/api';
const state = {
  token: getToken(),
  userInfo: {
    roles: []
  },
  roles: [],
  // 第一次加载菜单时用到
  loadMenus: false,

  routers: constantRouterMap,
  addRouters: [],
  sidebarRouters: [],

  // 假的登录用户信息
  userRes: {
    roles: ['admin'],
    user: {
      roles: [{ dataScope: '全部', id: 1, level: 1, name: '超级管理员' }],
      gender: '男',
      id: 1,
      email: 'admin@el-admin.vip',
      dept: { id: 2, name: '研发部' },
      avatarPath: '/home/eladmin/avatar/avatar.jpeg',
      nickName: '管理员',
      username: 'admin',
      phone: '13997592990'
    }
  }
};

const actions = {
  // 登录
  Login({ state, commit }: any, userInfo: any) {
    const rememberMe = userInfo.rememberMe;
    return new Promise((resolve, reject) => {
      Api.login(userInfo)
        .then((res: any) => {
          if (res.flag) {
            setToken(res.data.token, rememberMe);
            commit('SET_TOKEN', res.data.token);
            setUserInfo(res.data, commit);
            // 第一次加载菜单时用到， 具体见 src 目录下的 permission.js
            commit('SET_LOAD_MENUS', true);
            resolve(res.data);
          } else {
            reject(res);
          }
        })
        .catch(error => {
          reject(error);
        });
    });
  },

  // 获取用户信息
  GetInfo({ state, commit }: any) {
    return new Promise((resolve, reject) => {
      Api.getInfo()
        .then((res: any) => {
          setUserInfo(res.data, commit);
          resolve(res);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  // 登出
  LogOut({ commit }: any) {
    return new Promise((resolve, reject) => {
      logOut(commit);
      resolve({});

      // logout().then(res => {
      //   logOut(commit)
      //   resolve()
      // }).catch(error => {
      //   logOut(commit)
      //   reject(error)
      // })
    });
  },

  updateLoadMenus({ commit }: any) {
    return new Promise((resolve, reject) => {
      commit('SET_LOAD_MENUS', false);
    });
  },

  // 菜单设置
  GenerateRoutes({ commit }: any, asyncRouter: any) {
    commit('SET_ROUTERS', asyncRouter);
  },
  SetSidebarRouters({ commit }: any, sidebarRouter: any) {
    commit('SET_SIDEBAR_ROUTERS', sidebarRouter);
  }
};

const mutations = {
  SET_TOKEN: (state: any, token: any) => {
    state.token = token;
  },
  SET_USER: (state: any, user: any) => {
    Object.assign(state.userInfo, user);
  },
  SET_ROLES: (state: any, roles: any) => {
    state.roles = roles;
  },
  SET_LOAD_MENUS: (state: any, loadMenus: any) => {
    state.loadMenus = loadMenus;
  },

  // 设置路由
  SET_ROUTERS: (state: any, routers: any) => {
    state.addRouters = routers;
    state.routers = constantRouterMap.concat(routers);
  },
  // 设置菜单路由
  SET_SIDEBAR_ROUTERS: (state: any, routers: any) => {
    state.sidebarRouters = constantRouterMap.concat(routers);
  }
};

// 遍历后台传来的路由字符串，转换为组件对象
const modulesRoutes = import.meta.glob('/src/views/**/*.vue');
export const filterAsyncRouter = (routers: any, lastRouter = false, type = false) => {
  return routers.filter((router: any) => {
    if (type && router.children) {
      router.children = filterChildren(router.children);
    }
    if (router.component) {
      if (router.component === 'Layout') {
        // Layout组件特殊处理
        router.component = Layout;
      } else if (router.component === 'ParentView') {
        router.component = ParentView;
      } else {
        const component = router.component;
        router.component = modulesRoutes[`/src/views/${component}.vue`];
      }
    }
    if (router.children != null && router.children && router.children.length) {
      router.children = filterAsyncRouter(router.children, router, type);
    } else {
      delete router['children'];
      delete router['redirect'];
    }
    return true;
  });
};

function filterChildren(childrenMap: any, lastRouter = false) {
  var children: any = [];
  childrenMap.forEach((el: any, index: number) => {
    if (el.children && el.children.length) {
      if (el.component === 'ParentView') {
        el.children.forEach((c: any) => {
          c.path = el.path + '/' + c.path;
          if (c.children && c.children.length) {
            children = children.concat(filterChildren(c.children, c));
            return;
          }
          children.push(c);
        });
        return;
      }
    }
    if (lastRouter) {
      el.path = lastRouter['path'] + '/' + el.path;
    }
    children = children.concat(el);
  });
  return children;
}

export const logOut = (commit: any) => {
  commit('SET_TOKEN', '');
  commit('SET_ROLES', []);
  removeToken();
};
export const setUserInfo = (res: any, commit: any) => {
  // 如果没有任何权限，则赋予一个默认的权限，避免请求死循环
  if (res.roles.length === 0) {
    commit('SET_ROLES', ['ROLE_SYSTEM_DEFAULT']);
  } else {
    commit('SET_ROLES', res.roles);
  }
  commit('SET_USER', res);
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
