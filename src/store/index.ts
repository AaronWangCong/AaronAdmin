import { createStore } from "vuex";
import getters from "./getters";

const files = import.meta.globEager("./modules/*.ts");
const modules: any = {};
for (const key in files) {
  modules[key.replace(/(\.\/modules\/|\.ts)/g, "")] = files[key].default;
}

Object.keys(modules).forEach((item) => {
  modules[item]["namespaced"] = true;
});

export default createStore({
  getters,
  modules,
});
