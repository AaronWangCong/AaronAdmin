const getters = {
  sidebar: (state: any) => state.app.sidebar,
  device: (state: any) => state.app.device,

  token: (state: any) => state.permission.token,
  roles: (state: any) => state.permission.roles,
  userInfo: (state: any) => state.permission.userInfo,
  loadMenus: (state: any) => state.permission.loadMenus,
  permission_routers: (state: any) => state.permission.routers,
  addRouters: (state: any) => state.permission.addRouters,
  sidebarRouters: (state: any) =>
    state.permission.sidebarRouters.filter((i: any) => !i.hidden),

  visitedViews: (state: any) => state.tagsView.visitedViews,
  cachedViews: (state: any) => state.tagsView.cachedViews,
};

export default getters;
