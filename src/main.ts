import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

import theme from '@/utils/theme';
// 公共样式
import '@/assets/css/index.css';
// el-icon
import { Iconcomponents } from '@/utils/el-icons';
// motion动画
import { MotionPlugin } from '@vueuse/motion';
// 引入 全局注册组件
import PublicComponent from '@/components/componentInstall/componentInstall';

const app = createApp(App);
// 初始化主题
theme.init();
// 按需注册icon
Iconcomponents.forEach(component => app.component(component.name, component));

// 其他注册
app.use(MotionPlugin).use(PublicComponent).use(router).use(store).mount('#app');
