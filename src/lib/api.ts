import http from './http';

export let urlPrefix = '/oa';
export let uniPrefix = '/uni';
export let wyytPrefix = '/v1';

// 验证码
export function getCodeImg(data?: any) {
  return http({
    url: `${urlPrefix}/code`,
    data: data,
    method: 'get'
  });
}

// koa+nodeJs后台
// 查询文章列表
export function articleQuery(data?: any) {
  return http({
    url: `${urlPrefix}/categoryQuery`,
    data: data,
    method: 'post'
  });
}

export function isLogin(data?: any) {
  return http({
    url: `${urlPrefix}/user/isLogin`,
    data: data,
    method: 'get'
  });
}

export function login(data: any) {
  return http({
    url: `${wyytPrefix}/auth/login`,
    data: data,
    method: 'post'
  });
}

export function getInfo(data?: any) {
  return http({
    url: `${wyytPrefix}/auth/info`,
    data: data,
    method: 'get'
  });
}

export default {
  getCodeImg,
  articleQuery,
  isLogin,
  login,
  getInfo
};
