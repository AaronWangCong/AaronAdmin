import { AxiosRequestConfig } from "axios";
import axios from "@/utils/axios";

export default (config: AxiosRequestConfig) => {
  return new Promise((resolve, reject) => {
    let requestConfig = config;
    requestConfig.url = requestConfig.url || "";
    requestConfig.method = requestConfig.method || "get";

    if (requestConfig.url.startsWith("/uni")) {
      requestConfig["action"] = config.data.action;
    }

    switch (requestConfig.method.toUpperCase()) {
      case "GET":
        requestConfig.params = config.data;
        break;
      case "DELETE":
        requestConfig.params = config.data;
        break;
      case "POST":
        requestConfig.data = config.data;
        break;
      case "PUT":
        requestConfig.data = config.data;
        break;
    }

    axios(requestConfig)
      .then((resp) => {
        // console.log("request success");
        // console.log(requestConfig.url);
        resolve(resp);
      })
      .catch((err) => {
        console.log("request fail");
        console.log(requestConfig.url);
        console.log(err);
        reject(err);
      });
  });
};
