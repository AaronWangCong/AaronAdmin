import { nextTick, reactive, UnwrapRef } from 'vue';
import axios from '@/utils/axios';
import { AxiosRequestConfig } from 'axios';

/**
 * table表格use
 * @param config 请求配置 TableHookConfig
 * @param T 泛型参数T 列表数据行的类型
 * @param S 泛型参数S 列表查询条件对象的类型
 * @param config TableHookConfig
 * @returns TableHookRes
 */
export default function useTable<T = any, S = TableOptions>(config: TableHookConfig<S>): TableHookRes<T, S> {
  const defaultBeforeSearch = () => true;
  let sourceData: T[] = [];
  if (config.tableData) {
    sourceData = JSON.parse(JSON.stringify(config.tableData));
  }

  let hookRes = reactive({
    loadType: config.loadType ? config.loadType : 'always',
    tableOptions: JSON.parse(JSON.stringify(config.tableOptions || {})) as S,
    oldOptions: JSON.parse(JSON.stringify(config.tableOptions || {})) as S,
    sendOptions: {} as S,
    axiosConfig: JSON.parse(JSON.stringify(config.axiosConfig || {})) as AxiosRequestConfig,
    data: sourceData,
    loading: true,
    extendData: config.loadType === 'none' ? sourceData : null,
    pagination: { pageNo: 1, pageSize: 10, total: sourceData.length, layout: 'total,sizes, prev, pager, next, jumper' },
    beforeSearch: config.beforeSearch || defaultBeforeSearch,
    beforeFetch: config.beforeFetch,
    beforeResolve: config.beforeResolve,
    search: () => {
      return new Promise<void>(async (resolve, reject) => {
        // 查询前 校验参数
        if (typeof hookRes.beforeSearch === 'function') {
          const valid = await hookRes.beforeSearch();
          if (!valid) return resolve();
        }

        // 记录请求参数
        Object.keys(hookRes.tableOptions as S).forEach(key => {
          hookRes.sendOptions[key] = hookRes.tableOptions[key];
        });
        hookRes.pagination.pageNo === 1;
        await hookRes.fetchData();
        resolve();
      });
    },
    pageCurrentChange: (val: number) => {
      return new Promise<void>(async (resolve, reject) => {
        hookRes.pagination.pageNo = val;
        await hookRes.fetchData();
        resolve();
      });
    },
    pageSizeChange: async (val: number) => {
      return new Promise<void>(async (resolve, reject) => {
        hookRes.pagination.pageSize = val;
        if (Math.ceil(hookRes.pagination.total / val) >= hookRes.pagination.pageNo) {
          await nextTick();
          await hookRes.fetchData();
        }
        resolve();
      });
    },
    reset: (reload = true) => {
      return new Promise<void>(async (resolve, reject) => {
        Object.keys(hookRes.tableOptions as S).forEach(key => {
          hookRes.tableOptions[key] = hookRes.oldOptions[key];
        });
        if (reload) {
          await hookRes.search();
        }
        resolve();
      });
    },
    fetchData: () => {
      return new Promise<void>(async (resolve, reject) => {
        let options = JSON.parse(
          JSON.stringify({
            pagingDto: hookRes.pagination,
            ...(hookRes.sendOptions as S)
          })
        );

        // 发送请求前 可以执行beforeFetch钩子 可对请求参数修改处理
        if (typeof hookRes.beforeFetch === 'function') {
          const valid = await hookRes.beforeFetch(options);
          if (valid) {
            options = valid;
          } else {
            return resolve();
          }
        }

        // 处理请求参数
        hookRes.axiosConfig.method = hookRes.axiosConfig.method || 'post';
        const method = hookRes.axiosConfig.method.toLowerCase();

        let sendConfig = JSON.parse(JSON.stringify(hookRes.axiosConfig));
        // 合并请求参数 仅支持get、delete、post、put
        if (method === 'get' || method === 'delete') {
          if (hookRes.axiosConfig.params) {
            sendConfig.params = { ...hookRes.axiosConfig.params, ...options };
          } else {
            sendConfig.params = options;
          }
        }
        if (method === 'post' || method === 'put') {
          if (hookRes.axiosConfig.data) {
            sendConfig.data = { ...hookRes.axiosConfig.data, ...options };
          } else {
            sendConfig.data = options;
          }
        }

        // 发送请求
        let result: any = {
          flag: true, // true表示响应成功， false表示异常
          rows: []
        };
        hookRes.loading = true;

        if (hookRes.loadType === 'none') {
          // none表示表格数据从外部传入 不需要请求接口
          result.rows = sourceData;
        } else if (hookRes.loadType === 'once') {
          // once一次性请求完列表所有数据 不需要动态分页
          if (!hookRes.extendData) {
            // extendData记录的是原始数据
            result = await axios(sendConfig);
            hookRes.extendData = JSON.parse(JSON.stringify(result.rows));
          } else {
            result.rows = hookRes.extendData;
          }
        } else {
          // 否则 loadType==="always"表示每次都请求接口
          result = await axios(sendConfig);
          hookRes.extendData = JSON.parse(JSON.stringify(result.rows));
        }

        // 请求完成后 在返回数据前 执行beforeResolve  可对axios返回的数据做修改
        // 对于一次性加载的数据 如果需要分页 需要页面自己在beforeResolve里进行数据过滤
        if (typeof hookRes.beforeResolve === 'function') {
          const valid = await hookRes.beforeResolve(result.rows);
          if (valid) {
            result.rows = valid.rows;
          } else {
            return resolve();
          }
        }

        if (result) {
          hookRes.pagination.total = result.total || 0;
          hookRes.data = result.rows || [];
        } else {
          hookRes.pagination.total = 0;
          hookRes.data = [];
        }
        hookRes.loading = false;
        resolve();
      });
    }
  });
  return hookRes as TableHookRes<T, S>;
}
