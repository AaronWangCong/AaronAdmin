import type { DefineComponent } from 'vue';

const component = Object.create(null);
/* eslint-disable */
import Echart from './echart/index';
import WcTable from './wc-table/index';

component.install = function (vue: DefineComponent) {
  // ECharts 图表渲染
  vue.component('echart', Echart);
  // Wc-table 表格分页二次封装
  vue.component('WcTable', WcTable);
};
export default component;
