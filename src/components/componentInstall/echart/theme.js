const theme = {
  // animation: false,
  color: ['#4E6EF2', '#0AAB62', '#F43D25', '#FF6600', '#9A66E4', '#ed3f14'],
  backgroundColor: 'rgba(0,0,0,0)',
  grid: {
    // 距离上下左右的边距
    top: '15%',
    left: '1%',
    right: '1%',
    bottom: '3%',
    containLabel: true
  },
  title: {
    textStyle: {
      color: '#6c63ff'
    },
    subtextStyle: {
      color: '#a7a2fc'
    }
  },
  tooltip: {
    show: false //取消 鼠标滑过的提示框
  },
  legend: {
    x: 'left',
    selectedMode: false //取消图例的点击事件
  },
  line: {
    lineStyle: {
      width: '1',
      type: 'solid', //'dotted'虚线 'solid'实线
      color: '#6c63ff'
    },
    itemStyle: {
      // 小圆点样式
      color: '#6c63ff'
    },
    symbolSize: '6',
    symbol: 'emptyCircle', // 空心圆
    smooth: false
  },
  bar: {
    label: {
      show: true,
      position: 'top',
      color: '#6c63ff'
    },
    // barWidth: 40,
    itemStyle: {
      borderRadius: 3
    }
  },
  pie: {
    itemStyle: {
      borderWidth: 0,
      borderColor: '#fff'
    },
    emphasis: {
      itemStyle: {
        borderWidth: 0,
        borderColor: '#fff'
      }
    }
  },
  categoryAxis: {
    // 底部坐标
    axisLine: {
      show: true,
      lineStyle: {
        color: '#eeeeee'
      }
    },
    // axisLabel: {
    //   show: true,
    //   color: '#888888',
    //   fontSize: 12
    // },
    // splitLine: {
    //   show: false,
    //   lineStyle: {
    //     color: ['#eeeeee']
    //   }
    // },
    splitArea: {
      show: false,
      areaStyle: {
        color: ['rgba(250,250,250,0.05)', 'rgba(200,200,200,0.02)']
      }
    }
  },
  valueAxis: {
    // 左右坐标
    // axisLine: {
    //   show: true,
    //   lineStyle: {
    //     color: '#eeeeee'
    //   }
    // },
    // axisLabel: {
    //   // 左右刻度样式
    //   show: true,
    //   margin: 0,
    //   inside: true, // 刻度标签默认朝内
    //   color: '#888888'
    // },
    splitLine: {
      show: false,
      lineStyle: {
        color: '#eeeeee'
      }
    }
  }
};
export default theme;
