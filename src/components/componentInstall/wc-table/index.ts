// 导入组件，组件必须声明 name
import WcTable from './WcTable.vue';

// 为组件提供 install 安装方法，供按需引入
WcTable.install = function (app: any) {
  app.component(WcTable.name, WcTable);
};

// 默认导出组件
export default WcTable;
