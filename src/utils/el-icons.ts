import { Edit, Expand, Fold, Search, ArrowDownBold, ArrowDown } from '@element-plus/icons';

const Iconcomponents = [Edit, Expand, Fold, Search, ArrowDownBold, ArrowDown];

export { Iconcomponents };
