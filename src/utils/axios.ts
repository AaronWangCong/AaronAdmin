import axios, { AxiosInstance } from "axios";
import { ElMessage } from "element-plus";
import Router from "@/router";

let instance: AxiosInstance = axios.create();

instance.defaults.timeout = 60000;
instance.defaults.headers = instance.defaults.headers || {};

// 请求发出拦截器
instance.interceptors.request.use(
  (config) => {
    config.headers = config.headers || {};
    if (!config.headers["Authorization"]) {
      config.headers["Authorization"] =
        localStorage.getItem("Authorization") || "";
    }
    config.headers["Content-Type"] = "application/json";

    return config;
  },
  (error) => {
    ElMessage({ type: "error", message: "请求参数配置错误" });
    return Promise.resolve({ code: 500001, message: "请求参数配置错误" });
  }
);
// 请求响应拦截器
instance.interceptors.response.use(
  (res) => {
    if (res.data) {
      return Promise.resolve(res.data);
    } else {
      ElMessage({ type: "error", message: "服务器未响应数据" });
      return Promise.resolve({ code: 500002, message: "服务器未响应数据" });
    }
  },
  (error) => {
    let code = 500999,
      message = "未知错误";
    if (error.response) {
      code = error.response.status || 500999;
      switch (error.response.status) {
        case 400:
          message = error.response.data.message;
          break;
        case 401:
          message = "登录信息失效";
          localStorage.setItem("Authorization", "");
          Router.push("/login");
          break;
        case 403:
          message = "请求资源无权访问";
          break;
        case 404:
          message = "请求资源不存在";
          break;
        case 500:
          message = "服务器错误";
          break;
        case 504:
          message = "网关超时";
          break;
        default:
          message = "服务器异常" + error.response.status;
          break;
      }
    } else if (error.request) {
      // 已正常发出请求 但未收到响应
      if (error.message == "Network Error") {
        // Network Error
        code = 500003;
        message = "无法连接服务器";
      } else if (error.message.indexOf("timeout of") > -1) {
        // timeout of 1000ms exceeded
        code = 500004;
        message = "客户端请求超时";
      } else {
        code = 500005;
        message = "未收到响应";
      }
    } else {
      // 请求未发出就已报错  前端请求配置出问题
      code = 500006;
      message = "无效的请求";
    }
    ElMessage({ type: "error", message: message });
    return Promise.reject({ code: code, message: message });
  }
);

export default instance;
