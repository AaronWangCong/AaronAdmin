import Cookies from 'js-cookie';
import Config from '@/utils/config';

const TokenKey = Config.TokenKey;

export function getToken() {
  return localStorage.getItem(TokenKey);
}

export function setToken(token: string, rememberMe: boolean) {
  return localStorage.setItem(TokenKey, token);
}

export function removeToken() {
  return localStorage.setItem(TokenKey, '');
}
