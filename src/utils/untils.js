/**
 * 获取地址栏参数
 * @param name key
 */
function getQuery(name) {
  var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
  var r = null;
  if (window.location.search) {
    r = window.location.search.substr(1).match(reg);
  } else if (window.location.hash) {
    r = (window.location.hash.split('?')[1] || '').match(reg);
  } else {
    r = null;
  }
  if (r != null) return unescape(r[2]);
  return null;
}
/**
 * 设置cookie
 * @param name key
 * @param value 值
 * @param exdays 过期时间 天
 */
function setCookie(name, value, exdays) {
  if (exdays === void 0) {
    exdays = 365;
  }
  var d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  var expires = 'expires=' + d.toUTCString();
  document.cookie = name + '=' + value + '; ' + expires;
}
/**
 * 获取cookie
 * @param  name key
 */
function getCookie(name) {
  var _name = name + '=';
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') c = c.substring(1);
    if (c.indexOf(_name) != -1) return c.substring(_name.length, c.length);
  }
  return '';
}
/**
 * 清除cookie
 * @param name key
 */
function clearCookie(name) {
  setCookie(name, '', -1);
}
/**
 * 打开新的标签页
 * @param url 地址
 */
function openNewtab(url) {
  var a = document.createElement('a');
  a.setAttribute('href', url);
  a.setAttribute('target', '_blank');
  a.setAttribute('id', 'camnpr');
  document.body.appendChild(a);
  a.click();
}
/**
 * 设置localStorage数据，item值非json对象，请直接localStorage.setItem
 * @param {string} item 键名
 * @param {any} data 键值
 * @return {*}
 */
function setStorage(item, data) {
  if (!item || !data) return;
  localStorage.setItem(item, JSON.stringify(data));
}
/**
 * 获取localStorage数据，item值非json对象，请直接localStorage.setItem
 * @param item 键名
 */
function getStorage(item) {
  if (!item) return null;
  var localdata = localStorage.getItem(item);
  if (localdata !== null) {
    try {
      var formatData = JSON.parse(localdata);
      return formatData;
    } catch (e) {
      return localdata;
    }
  } else {
    return null;
  }
}
/**
 * 生成随机字符串
 * @param len 生成的字符串长度(不包含时间戳的长度)
 * @param time 是否要在生成的字符串前面加时间戳  默认为true
 */
function randomName(len, time) {
  if (len === void 0) {
    len = 16;
  }
  if (time === void 0) {
    time = true;
  }
  var chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';
  var maxPos = chars.length;
  var str = '';
  for (var i = 0; i < len; i++) {
    str += chars.charAt(Math.floor(Math.random() * maxPos));
  }
  if (time) {
    return new Date().getTime() + str;
  } else {
    return str;
  }
}
/**
 * 防抖函数
 * @param 实际要执行的函数
 * @param delay  延迟时间，单位是毫秒(ms)
 * @return 返回一个防抖函数
 */
function debounce(fn, delay) {
  if (fn === void 0) {
    fn = function () {};
  }
  // 定时器，用来 setTimeout
  var timer;
  // 返回一个函数，这个函数会在一个时间区间结束后的 delay 毫秒时执行 fn 函数
  return function () {
    // 保存函数调用时的上下文和参数，传递给 fn
    var context = this;
    var args = arguments;
    // 每次这个返回的函数被调用，就清除定时器，以保证不执行 fn
    clearTimeout(timer);
    // 当返回的函数被最后一次调用后（也就是用户停止了某个连续的操作），
    // 再过 delay 毫秒就执行 fn
    timer = setTimeout(function () {
      fn.apply(context, args);
    }, delay);
  };
}
/**
 * 节流函数
 * @param fn 实际要执行的函数
 * @param threshhold 执行间隔，单位是毫秒(ms)
 * @return 返回一个节流函数
 */
function throttle(fn, threshhold) {
  if (fn === void 0) {
    fn = function () {};
  }
  if (threshhold === void 0) {
    threshhold = 250;
  }
  // 记录上次执行的时间
  var last;
  // 定时器
  var timer;
  // 返回的函数，每过 threshhold 毫秒就执行一次 fn 函数
  return function () {
    // 保存函数调用时的上下文和参数，传递给 fn
    var context = this;
    var args = arguments;
    var now = +new Date();
    // 如果距离上次执行 fn 函数的时间小于 threshhold，那么就放弃
    // 执行 fn，并重新计时
    if (last && now < last + threshhold) {
      clearTimeout(timer);
      // 保证在当前时间区间结束后，再执行一次 fn
      timer = setTimeout(function () {
        last = now;
        fn.apply(context, args);
      }, threshhold);
      // 在时间区间的最开始和到达指定间隔的时候执行一次 fn
    } else {
      last = now;
      fn.apply(context, args);
    }
  };
}
/**
 * 遍历目标对象的每个key分别，从源对象获取数据并赋值，仅支持{}对象。
 * @param target 目标对象
 * @param source 源对象
 */
function allGive(target, source) {
  if (!isObject(target) || !isObject(source)) return;
  Object.keys(target).forEach(function (key) {
    if (source[key] !== undefined) {
      target[key] = source[key];
    }
  });
}
/**
 * 遍历目标数组 从源对象获取数据并赋值。
 * @param target array 目标对象
 * @param source {} 源对象
 * @param key 关联字段key
 * @param content 要修改的字段 字段名string 或数组
 */
function arrayGive(targrt, sources, key, content) {
  if (key === void 0) {
    key = 'prop';
  }
  if (content === void 0) {
    content = 'content';
  }
  targrt.forEach(function (element) {
    if (element.hasOwnProperty(key)) {
      if (isArray(content)) {
        content.forEach(function (contentItem) {
          if (element.hasOwnProperty(contentItem)) {
            element[contentItem] = sources[element[key]];
          }
        });
      } else {
        if (element.hasOwnProperty(content)) {
          element[content] = sources[element[key]];
        }
      }
    }
  });
}
/**
 * 是否为字符串
 * @param o 入参
 */
function isString(o) {
  return Object.prototype.toString.call(o).slice(8, -1) === 'String';
}
/**
 * 是否为数字
 * @param o 入参
 */
function isNumber(o) {
  return Object.prototype.toString.call(o).slice(8, -1) === 'Number';
}
/**
 * 是否为boolean
 * @param o 入参
 */
function isBoolean(o) {
  return Object.prototype.toString.call(o).slice(8, -1) === 'Boolean';
}
/**
 * 是否为函数
 * @param o 入参
 */
function isFunction(o) {
  return Object.prototype.toString.call(o).slice(8, -1) === 'Function';
}
/**
 * 是否为null
 * @param o 入参
 */
function isNull(o) {
  return Object.prototype.toString.call(o).slice(8, -1) === 'Null';
}
/**
 * 是否为undefined
 * @param o 入参
 */
function isUndefined(o) {
  return Object.prototype.toString.call(o).slice(8, -1) === 'Undefined';
}
/**
 * 是否为对象
 * @param o 入参
 */
function isObject(o) {
  return Object.prototype.toString.call(o).slice(8, -1) === 'Object';
}
/**
 * 是否为数组
 * @param o 入参
 */
function isArray(o) {
  return Object.prototype.toString.call(o).slice(8, -1) === 'Array';
}
/**
 * 是否为时间
 * @param o 入参
 */
function isDate(o) {
  return Object.prototype.toString.call(o).slice(8, -1) === 'Date';
}
/**
 * 是否为正则
 * @param o 入参
 */
function isRegExp(o) {
  return Object.prototype.toString.call(o).slice(8, -1) === 'RegExp';
}
/**
 * 是否为错误对象
 * @param o 入参
 */
function isError(o) {
  return Object.prototype.toString.call(o).slice(8, -1) === 'Error';
}
/**
 * 是否为Symbol函数
 * @param o 入参
 */
function isSymbol(o) {
  return Object.prototype.toString.call(o).slice(8, -1) === 'Symbol';
}
/**
 * 是否为Promise对象
 * @param o 入参
 */
function isPromise(o) {
  return Object.prototype.toString.call(o).slice(8, -1) === 'Promise';
}
/**
 * 是否为Set对象
 * @param o 入参
 */
function isSet(o) {
  return Object.prototype.toString.call(o).slice(8, -1) === 'Set';
}
/**
 * 是否为非真
 * @param o 入参
 */
function isFalse(o) {
  if (!o || o === isNull(o) || o === isUndefined(o) || o === false || o === isNaN(o)) return true;
  return false;
}
/**
 * 是否为真
 * @param o 入参
 */
function isTrue(o) {
  return !isFalse(o);
}
/**
 * 是否为空
 * @param o 入参
 */
function isEmpty(o) {
  if (o === '' || isNull(o) || o === isUndefined(o)) return true;
  return false;
}
/**
 * 获取设备类型
 * */
function getDeviceType() {
  var u = navigator.userAgent;
  if (u.indexOf('Android') > -1 || u.indexOf('Linux') > -1) {
    // 安卓
    return 'Android';
  } else if (u.indexOf('iPhone') > -1) {
    // 苹果
    return 'iPhone';
  } else if (u.indexOf('iPad') > -1) {
    // iPad
    return 'iPad';
  } else if (u.indexOf('Windows Phone') > -1) {
    // winphone手机
    return 'Windows Phone';
  } else {
    return 'other';
  }
}
/**
 * 是否为ios
 * @param o 入参
 */
function isIos() {
  var tag = getDeviceType();
  if (tag === 'iPhone') {
    return true;
  } else {
    return false;
  }
}
/**
 * 是否为Android
 */
function isAndroid() {
  var tag = getDeviceType();
  if (tag === 'Android') {
    return true;
  } else {
    return false;
  }
}
/**
 * 是否为PC端
 */
function isPC() {
  var userAgentInfo = navigator.userAgent;
  var Agents = ['Android', 'iPhone', 'SymbianOS', 'Windows Phone', 'iPad', 'iPod'];
  var flag = true;
  for (var v = 0; v < Agents.length; v++) {
    if (userAgentInfo.indexOf(Agents[v]) > 0) {
      flag = false;
      break;
    }
  }
  return flag;
}
/**
 * 获取浏览器类型
 */
function getBrowserType() {
  var userAgent = navigator.userAgent; // 取得浏览器的userAgent字符串
  var isOpera = userAgent.indexOf('Opera') > -1; // 判断是否Opera浏览器
  var isIE = userAgent.indexOf('compatible') > -1 && userAgent.indexOf('MSIE') > -1 && !isOpera; // 判断是否IE浏览器
  var isIE11 = userAgent.indexOf('Trident') > -1 && userAgent.indexOf('rv:11.0') > -1;
  var isEdge = userAgent.indexOf('Edge') > -1 && !isIE; // 判断是否IE的Edge浏览器
  var isFF = userAgent.indexOf('Firefox') > -1; // 判断是否Firefox浏览器
  var isSafari = userAgent.indexOf('Safari') > -1 && userAgent.indexOf('Chrome') == -1; // 判断是否Safari浏览器
  var isChrome = userAgent.indexOf('Chrome') > -1 && userAgent.indexOf('Safari') > -1; // 判断Chrome浏览器
  if (isIE) {
    var reIE = new RegExp('MSIE (\\d+\\.\\d+);');
    reIE.test(userAgent);
    var fIEVersion = parseFloat(RegExp['$1']);
    if (fIEVersion == 7) return 'IE7';
    else if (fIEVersion == 8) return 'IE8';
    else if (fIEVersion == 9) return 'IE9';
    else if (fIEVersion == 10) return 'IE10';
    else return 'IE7以下';
  }
  if (isIE11) return 'IE11';
  if (isEdge) return 'Edge';
  if (isFF) return 'FF';
  if (isOpera) return 'Opera';
  if (isSafari) return 'Safari';
  if (isChrome) return 'Chrome';
  return '';
}

export {
  allGive,
  arrayGive,
  clearCookie,
  debounce,
  getBrowserType,
  getCookie,
  getDeviceType,
  getQuery,
  getStorage,
  isAndroid,
  isArray,
  isBoolean,
  isDate,
  isEmpty,
  isError,
  isFalse,
  isFunction,
  isIos,
  isNull,
  isNumber,
  isObject,
  isPC,
  isPromise,
  isRegExp,
  isSet,
  isString,
  isSymbol,
  isTrue,
  isUndefined,
  openNewtab,
  randomName,
  setCookie,
  setStorage,
  throttle
};
