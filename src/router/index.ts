import { createRouter, createWebHistory, Router, RouteRecordRaw } from 'vue-router';
import { decode } from 'js-base64';
import { constantRouterMap, routesNew } from './router';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
import { filterAsyncRouter } from '@/store/modules/permission';
import Config from '@/utils/config';
import { getToken } from '@/utils/auth';
import Store from '@/store';

NProgress.configure({ showSpinner: false });

const router: Router = createRouter({
  // createWebHashHistory createWebHistory
  history: createWebHistory(),
  routes: [...constantRouterMap],
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else {
      return { top: 0 };
    }
  }
});

const whiteList = ['/login']; // no redirect whitelist

router.beforeEach((to, from, next) => {
  if (to.meta.title) {
    document.title = Config.title + ' - ' + to.meta.title;
  }
  NProgress.start();
  if (getToken()) {
    // 已登录且要跳转的页面是登录页
    if (to.path === '/login') {
      next({ path: '/' });
      NProgress.done();
    } else {
      if (Store.getters.roles.length === 0) {
        // 判断当前用户是否已拉取完user_info信息
        Store.dispatch('permission/GetInfo')
          .then(res => {
            // 拉取user_info
            // 动态路由，拉取菜单
            loadMenus(next, to);
          })
          .catch(() => {
            Store.dispatch('permission/LogOut').then(() => {
              location.reload(); // 为了重新实例化vue-router对象 避免bug
            });
          });
        // 登录时未拉取 菜单，在此处拉取
      } else if (Store.getters.loadMenus) {
        // 修改成false，防止死循环
        Store.dispatch('permission/updateLoadMenus');
        loadMenus(next, to);
      } else {
        next();
      }
    }
  } else {
    /* has no token*/
    if (whiteList.indexOf(to.path) !== -1) {
      // 在免登录白名单，直接进入
      next();
    } else {
      next(`/login?redirect=${to.fullPath}`); // 否则全部重定向到登录页
      NProgress.done();
    }
  }
});

export const loadMenus = (next: any, to: any) => {
  // buildMenus().then(res => {
  const res = routesNew;
  const sdata = JSON.parse(JSON.stringify(res));
  const rdata = JSON.parse(JSON.stringify(res));
  const sidebarRoutes = filterAsyncRouter(sdata);
  const rewriteRoutes = filterAsyncRouter(rdata, false, true);
  rewriteRoutes.push({
    path: '/:pathMatch(.*)',
    redirect: '/404',
    hidden: true
  });

  Store.dispatch('permission/GenerateRoutes', rewriteRoutes).then(() => {
    // 存储路由
    rewriteRoutes.forEach((route: RouteRecordRaw) => {
      router.addRoute(route); // 动态添加可访问路由表
    });
    next({ ...to, replace: true });
  });
  Store.dispatch('permission/SetSidebarRouters', sidebarRoutes);
  // })
};

router.afterEach(() => {
  NProgress.done();
});

export default router;
