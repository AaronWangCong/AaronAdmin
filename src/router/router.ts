import Layout from '../layout/Index.vue';
import RouteView from '../components/RouteView.vue';

const constantRouterMap = [
  {
    path: '/login',
    name: 'Login',
    hidden: true,
    meta: {
      title: '登录'
    },
    component: () => import('../views/login/Login.vue')
  },
  {
    path: '/404',
    component: () => import('../views/features/404.vue'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('../views/features/401.vue'),
    hidden: true
  },
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path*',
        component: () => import('../views/features/redirect.vue')
      }
    ]
  },
  {
    path: '/',
    name: 'Layout',
    redirect: '/dashboard',
    component: Layout,
    children: [
      {
        path: 'dashboard',
        name: 'Dashboard',
        meta: {
          title: '首页',
          icon: 'icon-all-fill',
          affix: true
        },
        component: () => import('../views/Index.vue')
      }
    ]
  },
  {
    path: '/user',
    hidden: true,
    redirect: 'noredirect',
    component: Layout,
    children: [
      {
        path: 'center',
        name: 'Center',
        meta: {
          title: '个人中心'
        },
        component: () => import('../views/admin/User.vue')
      }
    ]
  }
];

const routesNew = [
  {
    path: '/zujian',
    name: 'Zujian',
    component: 'Layout',
    redirect: 'noredirect',
    meta: {
      title: '组件展示',
      icon: 'icon-databaseset-fill'
    },
    children: [
      {
        path: 'datav',
        name: 'Datav',
        meta: {
          title: '数据统计'
        },
        component: 'zujian/Datav'
      },
      {
        path: 'tables',
        name: 'Tables',
        meta: {
          title: '表格数据'
        },
        component: 'zujian/Tables'
      }
    ]
  },
  {
    path: '/technology',
    name: 'Technology',
    component: 'Layout',
    redirect: 'noredirect',
    meta: {
      title: '前沿技术栈',
      icon: 'icon-map-fill'
    },
    children: [
      {
        path: 'formEngine',
        name: 'FormEngine',
        meta: {
          title: '表单引擎'
        },
        component: 'technology/FormEngine'
      },
      {
        path: 'threeJs',
        name: 'ThreeJs',
        meta: {
          title: '3D引擎'
        },
        component: 'technology/ThreeJs'
      }
    ]
  },
  {
    path: '/amusing',
    name: 'Amusing',
    component: 'Layout',
    redirect: 'noredirect',
    meta: {
      title: '好物推荐',
      icon: 'icon-smile-fill'
    },
    children: [
      {
        path: 'unDraw',
        name: 'UnDraw',
        meta: {
          title: 'unDraw插画库'
        },
        component: 'amusing/UnDraw'
      },
      {
        path: 'motion',
        name: 'Motion',
        meta: {
          title: 'motion动画库'
        },
        component: 'amusing/Motion'
      }
    ]
  }
];

export { constantRouterMap, routesNew };
