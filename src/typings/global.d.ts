interface Login {
  username: string;
  password: string;
  code: number | null;
}
